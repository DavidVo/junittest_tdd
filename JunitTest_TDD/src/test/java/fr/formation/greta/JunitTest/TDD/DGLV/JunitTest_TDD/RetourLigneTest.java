package fr.formation.greta.JunitTest.TDD.DGLV.JunitTest_TDD;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class RetourLigneTest {
	
	private final int NB_MAX_CARATERES = 5;
	
	@Test
	public void travailleRetourneUneChaineNonNulle() {
	String lResultat = RetourLigne.travaille("Mignonne, allons voir si la rose\n" +
			"Qui ce matin avoit desclose\n" +
			"Sa robe de pourpre au Soleil,\n" +
			"A point perdu ceste vesprée\n" +
			"Les plis de sa robe pourprée,\n" +
			"Et son teint au vostre pareil.", NB_MAX_CARATERES);
	assertNotEquals(null, lResultat);
	}
	
	@Test
	public void travailleCoupeUneligneAssezCourteEnDeux() {
	String lResultat = RetourLigne.travaille("Qui ce ", NB_MAX_CARATERES);
	assertEquals("Qui c\ne ", lResultat);
	}
	
	@Test
	public void pasDeRetourLigneDansUneLigneCourte() {
	String lResultat = RetourLigne.travaille("Qui", NB_MAX_CARATERES);

	assertEquals("Qui", lResultat);
	}
	
	@Test
	public void lesLignePlusLonguesDoiventInclure2Retours() {
	String lResultat = RetourLigne.travaille("Qui ce matin", NB_MAX_CARATERES);
	assertEquals("Qui c\ne mat\nin", lResultat);
	}
	
	@Test
	public void lesLignePlusLonguesDoiventInclure3Retours() {
	String lResultat = RetourLigne.travaille("Qui ce matin avoit", NB_MAX_CARATERES);
	assertEquals("Qui c\ne mat\nin av\noit", lResultat);
	}
	
	@Test
	public void lesLignesNeDoiventPasNecessairementEtreMultipleDeNB_MAX_CARATERES() {
	String lResultat = RetourLigne.travaille("Qui ce matin avoit desclose", NB_MAX_CARATERES);
	assertEquals("Qui c\ne mat\nin av\noit d\nesclo\nse", lResultat);
	}



}



