package fr.formation.greta.JunitTest.TDD.DGLV.JunitTest_TDD;

public class RetourLigne {
	
	public static String travaille(final String str, final int nb) {
		if (str.length()<=3) return str;
		StringBuilder lBuilder  = new StringBuilder();
		for (int compt = 0; compt<= str.length()-1; compt+=nb) {
			if(compt+5 > str.length()) {lBuilder.append(str,compt,str.length());}
			else {lBuilder.append(str,compt,compt+nb);
			lBuilder.append("\n");}
		}
		return lBuilder.toString();
	}
}